<?php

namespace Drupal\simple_integrations\Controller;

use Drupal\simple_integrations\IntegrationInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;

/**
 * Connection test controller.
 *
 * Performs a straightforward connection test and prints a status message to the
 * user with the results. Once complete, redirects the user to the main listing.
 */
class ConnectionTestController extends ConnectionController {

  /**
   * Configure the connection test.
   *
   * @param \Drupal\simple_integrations\IntegrationInterface $integration
   *   Integration entity interface.
   *
   * @return \Symfony\Component\HttpFoundation\RedirectResponse
   *   A redirect response to the Integration entity collection.
   */
  public function configureConnectionTest(IntegrationInterface $integration) {
    // Create the connection.
    $this->connection->setIntegration($integration);
    $this->connection->configure();

    // Run the connection test.
    $this->runConnectionTest($integration);

    $collection_url = $integration->toUrl('collection')->toString();
    return new RedirectResponse($collection_url);
  }

  /**
   * Test the connection.
   *
   * @param \Drupal\simple_integrations\IntegrationInterface $integration
   *   The integration to run a connection test on.
   */
  public function runConnectionTest(IntegrationInterface $integration) {
    // Log that the connection test has started.
    /** @var \Drupal\simple_integrations\Entity\Integration $integration */
    if ($integration->isDebugMode()) {
      $started_message = $this->t('Connection test for %id started.', ['%id' => $integration->get('id')]);
      $integration->logDebugMessage($started_message);
    }

    try {
      // Perform a connection test and construct a response.
      $result = $integration->performConnectionTest($this->connection);
    }
    catch (\Exception $e) {
      $result = [
        'code' => $e->getCode(),
        'message' => $e->getMessage(),
      ];
    }

    $notice_type = $this->determineResultType($result['code']);
    $message = $this->t('Connection test complete. Returned code %code with message %message.', [
      '%code' => $result['code'],
      '%message' => $result['message'],
    ]);

    if ($integration->isDebugMode()) {
      // Log that the connection test has ended, with the results.
      $log_notice = ($notice_type == $this->messenger()::TYPE_ERROR) ? 'error' : 'notice';
      $integration->logDebugMessage($message, $log_notice);
    }

    $this->messenger()->addMessage($message, $notice_type);
  }

  /**
   * Determine what kind of result this was: positive, negative, or who knows?
   *
   * @param int $code
   *   The status code returned by \Drupal::httpClient(), or exception code
   *   returned by Guzzle.
   *
   * @return string
   *   'notice', 'error' or 'warning' depending on given code.
   */
  public function determineResultType($code = 200) {
    if ($code == 200) {
      // 200 is 'okay'; treat as a success.
      return $this->messenger()::TYPE_STATUS;
    }
    elseif (substr($code, 0, 1) === '3') {
      // 300 is 'redirected' or 'moved'; we found it, but it's not perfect.
      // Treat as a notice.
      return $this->messenger()::TYPE_WARNING;
    }
    else {
      // Anything else can be treated as an error.
      return $this->messenger()::TYPE_ERROR;
    }
  }

}
