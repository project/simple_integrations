<?php

namespace Drupal\simple_integrations;

use GuzzleHttp\Client;
use Drupal\simple_integrations\Exception\CertificateFileNotFoundException;
use Drupal\simple_integrations\Exception\IntegrationInactiveException;

/**
 * The connection client.
 *
 * Acts as an extension of core's http_client service, with additional
 * functionality to configure the request automatically.
 */
class ConnectionClient extends Client {

  /**
   * The associated Integration.
   *
   * @var \Drupal\simple_integrations\IntegrationInterface
   */
  private $integration;

  /**
   * The authentication type.
   *
   * @var string
   */
  private $authType;

  /**
   * The location of a certificate.
   *
   * @var string
   */
  private $certificate;

  /**
   * The authentication username.
   *
   * @var string
   */
  private $authUser;

  /**
   * The authentication password or key.
   *
   * @var string
   */
  private $authKey;

  /**
   * The request end point.
   *
   * @var string
   */
  private $endPoint;

  /**
   * The timeout value, in milliseconds.
   *
   * @var int
   */
  private $timeout;

  /**
   * The credentials skip.
   *
   * @var bool
   */
  private $credentialsSkip = FALSE;

  /**
   * Config for the connection.
   *
   * @var array
   */
  private $requestConfig = [];

  /**
   * Perform a callback.
   *
   * @param string $method
   *   The method, eg GET or POST.
   * @param array $args
   *   An array of arguments, with a URI first and an array of config second.
   *
   * @return \Symfony\Component\HttpFoundation\Response
   *   The response.
   *
   * @throws \Drupal\simple_integrations\Exception\IntegrationInactiveException
   * @throws \GuzzleHttp\Exception\ClientException
   */
  public function __call($method, $args) {
    if (!$this->integration->isActive()) {
      throw new IntegrationInactiveException($this->integration->id());
    }

    return parent::__call($method, $args);
  }

  /**
   * Set the integration entity.
   *
   * @param \Drupal\simple_integrations\IntegrationInterface $integration
   *   The integration entity.
   *
   * @return \Drupal\simple_integrations\ConnectionClient
   *   Return this object.
   */
  public function setIntegration(IntegrationInterface $integration) {
    $this->integration = $integration;
    return $this;
  }

  /**
   * Get the integration entity.
   *
   * @return \Drupal\simple_integrations\IntegrationInterface
   *   The integration entity.
   */
  public function getIntegration() {
    return $this->integration;
  }

  /**
   * Set authentication type from a given integration.
   *
   * @return \Drupal\simple_integrations\ConnectionClient
   *   Return this object.
   */
  public function setAuthType() {
    $this->authType = $this->integration->get('auth_type');
    return $this;
  }

  /**
   * Get the authentication type.
   *
   * @return string
   *   The auth type.
   */
  public function getAuthType() {
    return $this->authType;
  }

  /**
   * Set a certificate.
   *
   * @return \Drupal\simple_integrations\ConnectionClient
   *   Return this object.
   *
   * @throws \Drupal\simple_integrations\Exception\CertificateFileNotFoundException
   */
  public function setCertificate() {
    $this->certificate = $this->integration->get('certificate');

    if (!empty($this->getCertificate())) {
      // If there is a certificate, make sure the file exists.
      if (!file_exists($this->getCertificate())) {
        throw new CertificateFileNotFoundException();
      }
    }

    return $this;
  }

  /**
   * Get the certificate file.
   *
   * @return string
   *   The certificate file.
   */
  public function getCertificate() {
    return $this->certificate;
  }

  /**
   * Set the authentication values from a given integration.
   *
   * @return \Drupal\simple_integrations\ConnectionClient
   *   Return this object.
   */
  public function setAuthValues() {
    $this->authUser = $this->integration->get('auth_user');
    $this->authKey = $this->integration->get('auth_key');
    return $this;
  }

  /**
   * Get the authentication user.
   *
   * @return string
   *   The auth user.
   */
  public function getAuthUser() {
    return $this->authUser;
  }

  /**
   * Get the authentication key.
   *
   * @return string
   *   The auth key.
   */
  public function getAuthKey() {
    return $this->authKey;
  }

  /**
   * Set the end point for this request.
   *
   * @return \Drupal\simple_integrations\ConnectionClient
   *   Return this object.
   */
  public function setRequestEndPoint() {
    $this->endPoint = $this->integration->get('external_end_point');
    return $this;
  }

  /**
   * Get the end point for this request.
   *
   * @return string
   *   The end point.
   */
  public function getRequestEndPoint() {
    return $this->endPoint;
  }

  /**
   * Set the timeout value for this request.
   *
   * @return \Drupal\simple_integrations\ConnectionClient
   *   Return this object.
   */
  public function setTimeout() {
    $this->timeout = $this->integration->get('timeout');
    return $this;
  }

  /**
   * Get the timeout value for this request.
   *
   * @return int
   *   The timeout.
   */
  public function getTimeout() {
    return $this->timeout;
  }

  /**
   * Set the credentials skip.
   *
   * @return \Drupal\simple_integrations\ConnectionClient
   *   Return this object.
   */
  public function setCredentialsSkip() {
    $this->credentialsSkip = $this->integration->get('skip_credentials');
    return $this;
  }

  /**
   * Get the credential skip for this request.
   *
   * @return bool
   *   Whether to skip adding credentials.
   */
  public function getCredentialsSkip() {
    return $this->credentialsSkip;
  }

  /**
   * Set configuration for a request.
   *
   * @param array $config
   *   An array of configuration.
   * @param bool $override
   *   If true, override all existing request configuration. Defaults to FALSE.
   */
  public function setRequestConfig(array $config, bool $override = FALSE) {
    if (!$override) {
      $config = $this->requestConfig + $config;
    }

    $this->requestConfig = $config;
  }

  /**
   * Get the configuration for a request.
   *
   * @return array
   *   The configuration.
   */
  public function getRequestConfig() {
    return $this->requestConfig;
  }

  /**
   * Perform configuration tasks.
   */
  public function configure() {
    // Configure the authentication. Always add these values, even if they're
    // blank.
    $this
      ->setAuthType()
      ->setAuthValues()
      ->setCertificate();

    // Configure the request end point.
    $this->setRequestEndPoint();

    // Set the timeout.
    $this->setTimeout();

    // If there is an auth type set, and the integration doesn't have "skip
    // adding credentials" set, then set the credentials.
    if ($this->getAuthType() != 'none' && !$this->getCredentialsSkip()) {
      $this->setCredentials();
    }
  }

  /**
   * Add credentials to a request.
   *
   * @return \Drupal\simple_integrations\ConnectionClient
   *   Return this object.
   */
  public function setCredentials() {
    if ($this->getAuthType() == 'headers') {
      // Add authentication headers.
      $this->requestConfig['headers']['Authorization'] = [
        $this->getAuthUser(),
        $this->getAuthKey(),
      ];
    }
    elseif ($this->getAuthType() == 'basic_auth') {
      // Add basic authentication.
      $this->requestConfig['auth'] = [
        $this->getAuthUser(),
        $this->getAuthKey(),
      ];
    }
    elseif ($this->getAuthType() == 'certificate') {
      // Add the certificate.
      $this->requestConfig['cert'] = [
        $this->getCertificate(),
        $this->getAuthKey(),
      ];
    }

    return $this;
  }

}
