<?php

namespace Drupal\simple_integrations\Exception;

use InvalidArgumentException as InvalidArgument;

/**
 * Invalid argument exception.
 */
class InvalidArgumentException extends InvalidArgument {

}
